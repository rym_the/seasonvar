const express = require('express')
const cors = require('cors');
const app = express()
const port = process.env.SERVER_PORT || 8089
const api = require('./seasonvar.js')

app.use(cors());

app.get('/shows', async (req, res) => {
    const list = await api.getShows();
    res.status(200).type('json');
    res.send(list);
})

app.get('/shows/:showId', async (req, res) => {
  const seasons = await api.getShowSeasons(parseInt(req.params['showId']));
  res.status(200).type('json');
  res.send(seasons);
})

app.get('/shows/:showId/:seasonId', async (req, res) => {
  const episodes = await api.getEpisodes(parseInt(req.params['seasonId']), parseInt(req.params['showId']));
  res.status(200).type('json');
  res.send(episodes);
})
app.get('/*', (req, res) => {
	res.status(400).type('html');
	res.send('<html><head><title>Error</title></head><body><h2>Error</h2><p>Used only for API</p></body></html>')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))