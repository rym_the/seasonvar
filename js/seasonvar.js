const puppeteer = require('puppeteer');
const config = require('./config')

module.exports = {
	getShows: async() => {
		const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
		const page = await browser.newPage();
		  
		await page.goto(config.baseUrl, {waitUntil: 'networkidle2'});
		let list = await page.evaluate(function() {
			let list = {};
		    $("[data-serial=list] a[data-id]").each(function(ind, link) {
		        let id = Number($(link).data('id'));
		        let href = $(link).attr('href');
		        let title = $(link).text();
		        list[id] = {
		            id: id,
		            title: title,
		            link: href
		        };
		    });
	    	return list;
		});
	    await browser.close();
	    return list;
	},
	getShowSeasons: async (showId) => {
		showId = Number(showId)
		const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  		const page = await browser.newPage();
		const list = await module.exports.getShows();

		await page.goto(config.baseUrl + list[showId.toString()].link, {waitUntil: 'networkidle2'});
		let seasons = await page.evaluate(function() {
	        var list = {
	        	title: $("h1.pgs-sinfo-title").text().split('/')[0].replace('Сериал',''),
	        	icon: $('.pgs-sinfo-img img[itemprop=thumbnailUrl]').prop('src'),
	        	seasons: {}
	        };
	        $(".pgs-seaslist .tabs-result .act h2 > a").each(function(ind, link) {
	            let href = $(link).attr('href');
	            let idMatch = $(link).text().match(/(\d+) сезон/);
	            if (!idMatch) {
	                return;
	            }
	            let id = Number(idMatch[1]);
	            list.seasons[id] = {
	                id: id,
	                link: href
	            };
	        });
        	return list;
	    });
	    seasons.id = showId;
  		await browser.close();
  		return seasons;
	},
	getEpisodes: async(seasonId, showId) => {
		seasonId = Number(seasonId)
		showId = Number(showId)
		const list = await module.exports.getShowSeasons(showId);
		let url = config.baseUrl + list.seasons[seasonId.toString()].link;
		const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  		const page = await browser.newPage();
  		await page.goto(url, {waitUntil: 'networkidle2'});

	    await page.evaluate(function() {
	        if (!$("[data-click=playerSwich]").hasClass('act')) {
	            $("[data-click=playerSwich]").click();
	        }
	    });
	    //await page.waitForNavigation();
	    const playlistUrl = await page.evaluate(function () {
            let ind = 0;
            let pcnt = 0;
            $(".pgs-trans [data-click=translate]").each(function(ind, el) {
                var currentPercentage = $(el).data('translate-percent') || 0;
                if (currentPercentage > pcnt) {
                    pcnt = currentPercentage;
                    ind = $(el).data('translate');
                }
            });
            return window.pl[ind];
        });
        await browser.close();
        return module.exports.loadPlaylist(playlistUrl);
    },
    loadPlaylist: async (playlistUrl) => {
	    if (!playlistUrl.match(/^https?::/)) {
	        playlistUrl = config.baseUrl + playlistUrl;
	    }
	    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  		const page = await browser.newPage();
  		await page.goto(playlistUrl, {waitUntil: 'networkidle2'});

    	
	    let playlist = await page.evaluate(function() {
            return JSON.parse(document.body.innerHTML);
	    });
	    await browser.close();
	    return playlist;
	}
}