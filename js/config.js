module.exports = {
    baseUrl : 'http://seasonvar.ru',
    listFile: 'data/list.json',
    playlistDirectory: 'data/playlist/',
    postfix: 60 * 10, //10 minutes
    urlMap: {
        '/shows': 'getShows',
        '/episodes': 'getSeasons.js'
    }
}