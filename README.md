# Seasonvar

Run API server with `node`

```
> node js/server.js
```

### Suitable routes

* /shows
* /shows/:showId
* /shows/:showId/:seasonId