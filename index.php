<?php



class simplePageApplication
{
    protected $config = [
        'baseUrl' => 'http://seasonvar.ru',
        'listFile' => 'data/list.json',
        'playlistDirectory' => 'data/playlist/',
        'postfix' => 60 * 10 //10 minutes
    ];

    private $commands = [
        'list' => 'getShows.js',
        'season' => 'getSeasons.js %d',
        'playlist' => 'getPlaylist.js %d %d',
    ];

    protected $timestamp;

    protected $uriParts = [];

    protected $bin = '/opt/phantomjs-2.1.1-linux-x86_64/bin/phantomjs --load-images=false';

    public function __construct()
    {
        $this->timestamp = !empty($_REQUEST['REQUEST_TIME']) ? $_REQUEST['REQUEST_TIME'] : time();
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        foreach ($uri as $part) {
            if (!empty($part)) {
                array_push($this->uriParts, $part);
            }
        }
    }

    public function run()
    {
        $renderer = new simpleTemplateManager();
        switch (count($this->uriParts)) {
            case 0:
                $output = $this->getList();
                $renderer->render('list', [$output]);
                break;
            case 1:
                $output = $this->getShow($this->uriParts[0]);
                $renderer->render('show', [$this->uriParts[0], $output]);
                break;
            case 2 :
                $output = $this->getPlaylist($this->uriParts[0], $this->uriParts[1]);
                $renderer->render('playlist', [$output]);
                break;
            default:
                echo 'FUCK';
        }
    }

    protected function getList()
    {
        if (!file_exists($this->config['listFile'])) {
            passthru($this->getCommand('list'));
        }
        return file_get_contents($this->config['listFile']);

    }

    protected function getShow($id)
    {
        $id = (int) $id;
        $list = $this->getList();
        $data = json_decode($list);
        if (empty($data->$id->seasons)) {
            passthru($this->getCommand('season', [$id]));
            $list = $this->getList();
            $data = json_decode($list);
            if (empty($data->$id->seasons)) {
                throw new \Exception("Wrong show ID: {$id}");
            }
        }
        return json_encode($data->$id->seasons);
    }

    protected function getPlaylist($showId, $seasonId)
    {
        $showId = (int) $showId;
        $seasonId = (int) $seasonId;
        $postfix = ceil($this->timestamp / $this->config['postfix']);
        $filename = sprintf(
            "%s%d_%d_%s.json",
            $this->config['playlistDirectory'],
            $showId,
            $seasonId,
            $postfix
        );
        if (!file_exists($filename)) {
            passthru($this->getCommand('playlist', [$showId, $seasonId]));
            if (!file_exists($filename)) {
                throw new \Exception("No playlist for {$showId}:{$seasonId}");
            }
        }
        return file_get_contents($filename);
    }

    protected function getCommand($command, $args = [])
    {
        if (!isset($this->commands[$command])) {
            throw new \Exception("Wrong command {$command}");
        }
        $commandArgs = call_user_func_array('sprintf', array_merge([$this->commands[$command]], $args));
        return escapeshellcmd($this->bin) . ' ' . $commandArgs;
    }
}


class simpleTemplateManager
{
    protected $js = [];

    protected $css = [];

    protected $body = [];

    public function __construct()
    {
//        array_push($this->js, 'https://cdn.jsdelivr.net/npm/lodash@4.17.4/lodash.min.js');
//        array_push($this->js, 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.slim.js');
    }

    public function addJs($file) {
        array_push($this->js, $file);
    }

    public function addCss($file) {
        array_push($this->css, $file);
    }

    public function render($action, $data)
    {
        switch ($action) {
            case 'list':
                call_user_func_array([$this, 'getList'], $data);
                break;
            case 'show':
                call_user_func_array([$this, 'getShow'], $data);
                break;
            case 'playlist':
                call_user_func_array([$this, 'getPlaylist'], $data);
                break;
        }
        echo $this->renderHtml();
    }
    protected function getBody()
    {
        return implode('', $this->body);
    }

    protected function getHeaderCss()
    {
        $css = '';
        foreach (array_unique($this->css) as $c) {
            $css .= "<link rel='stylesheet' href='{$c}' />" . PHP_EOL;
        }
        return $css;
    }

    protected function getHeaderJs()
    {
        $js = '';
        foreach (array_unique($this->js) as $j) {
            $js .= "<script src='{$j}' type='text/javascript' ></script>" . PHP_EOL;
        }
        return $js;
    }

    protected function renderHtml()
    {
        $return = '<html><head>';
        $return .= '<title>Seasonvar</title>';
        $return .= $this->getHeaderCss();
        $return .= $this->getHeaderJs();
        $return .= '</head><body>';
        $return .= $this->getBody();
        $return .='</body>';
        return $return;
    }

    protected function getList($data)
    {
        $data = json_decode($data);
        $return = '<ul>';
        foreach ($data as $id => $item) {
            $return .= '<li>';
            $return .= "<a href='/{$id}'>$item->title</a>";
            $return .= '</li>';
        }
        $return .= '</ul>';
        $this->appendBody($return);
    }

    protected function getShow($showId, $data)
    {
        $data = json_decode($data);
        $return = '<ul>';
        foreach ($data as $id => $item) {
            $return .= '<li>';
            $return .= "<a href='/{$showId}/{$id}/'>Сезон {$id}</a>";
            $return .= '</li>';
        }
        $return .= '</ul>';
        $this->appendBody($return);
    }

    protected function getPlaylist($data)
    {
        $data = json_decode($data);
        $return = '<ul>';
        foreach ($data as $id => $item) {
            $return .= '<li>';
            $return .= "<a href='{$item->file}'>{$item->title}</a>";
            $return .= '</li>';
        }
        $return .= '</ul>';
        $this->appendBody($return);
    }
    public function appendBody($body)
    {
        array_push($this->body, $body);
    }

}
$runner = new simplePageApplication();
$runner->run();